#include "stdafx.h"
#include "MessageList.h"


MessageList::MessageList()
{
	InitializeCriticalSectionAndSpinCount(&criticalSection, 0x00000400);
}


MessageList::~MessageList()
{
}

void MessageList::Add(string str, int id)
{
	EnterCriticalSection(&criticalSection);
	Message message = Message(str, id);
	messages.push_back(message);
	LeaveCriticalSection(&criticalSection);
}

vector<Message> MessageList::GetLatest(int index)
{
	EnterCriticalSection(&criticalSection);
	vector<Message> tmp = vector<Message>();
	while (index < messages.size()) {
		tmp.push_back(messages[index]);
		index++;
	}
	LeaveCriticalSection(&criticalSection);
	
	return tmp;
}

int MessageList::GetLastMessageIndex() {
	int size;

	EnterCriticalSection(&criticalSection);
	size =  messages.size();
	LeaveCriticalSection(&criticalSection);

	return size;
}