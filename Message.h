#pragma once

#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Message
{
public:
	Message();
	Message(string str, int id);
	~Message();

	void SetText(string str);
	string GetText();
	void SetId(int id);
	int GetId();

private:
	string text;
	int id;
};

