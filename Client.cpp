#include "stdafx.h"
#include "Client.h"

#define BUF_SIZE 255

Client::Client()
{
}

Client::Client(SOCKET socket, MessageList * messages)
{
	this->socket = socket;
	this->messageListPointer = messages;
	setLastMessageIndex();
}

Client::~Client()
{
}

void Client::StoreMessage(string message)
{
	messageListPointer->Add(message, GetId());
}

vector<Message> Client::GetLatestMessages(){
	vector<Message> messages;

	messages = messageListPointer->GetLatest(lastMessageIndex);
	lastMessageIndex += messages.size();
	
	return messages;
}

int Client::GetId()
{
	return (int) this->socket;
}

DWORD Client::Receive(LPVOID lpParam)
{
	int result;
	char buffer[BUF_SIZE];

	Client* client = (Client*)lpParam;

	// Make sure there is a console to receive output results. 
	client->stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (client->stdoutHandle == INVALID_HANDLE_VALUE)
		return 1;

	client->stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);

	string message;
	while (true) {
		// Empty message buffer
		message = "";

		// Receive first part of the message
		result = recv(client->socket, buffer, BUF_SIZE, 0);

		// Read till incoming buffer is still full
		u_long waitingBytes = 0;
		ioctlsocket(client->socket, FIONREAD, &waitingBytes);
		while (waitingBytes > 0) {
			message.append(Client::extractString(buffer, result));
			result = recv(client->socket, buffer, BUF_SIZE, 0);
			ioctlsocket(client->socket, FIONREAD, &waitingBytes);
		}

		if (result > 0) {
			message.append(Client::extractString(buffer, result));
		}

		client->StoreMessage(message);

		cout << "Message: " << message << endl;
		
		if (result < 0) {
			closesocket(client->socket);
			return 0;
		}
	}

	return 0;
}

DWORD Client::Send(LPVOID lpParam)
{
	int result;
	int lastMessageIndex = 0;
	vector<Message> messages;

	Client* client = (Client*)lpParam;

	// Make sure there is a console to receive output results.
	client->stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);

	if (client->stdoutHandle == INVALID_HANDLE_VALUE)
		return 1;

	while (true) {
		// Read from shared message list
		messages = client->GetLatestMessages();
		for (int i = 0; i < messages.size(); i++) {
			// Send messages to client
			if (messages[i].GetId() != client->GetId())
			{
				result = send(client->socket, messages[i].GetText().c_str(), (int)strlen(messages[i].GetText().c_str()), 0);

				if (result == SOCKET_ERROR) {
					closesocket(client->socket);
					return 1;
				}
			}
		}

		Sleep(1000);
	}

	return 0;
}

string Client::extractString(char* recvbuf, int size)
{
	// Allocate memory of temporary char array
	char *substr = (char*)malloc(sizeof(char)*(size + 1));
	// Copy the particular part of buffer to temporary char array
	strncpy_s(substr, sizeof(char)*(size + 1), recvbuf, sizeof(char)*size);
	substr[size] = '\0';
	// Create string representation
	string str = string(substr);
	// Free used memory
	free(substr);
	// Return the string representation
	return string(str);
}

void Client::setLastMessageIndex()
{
	lastMessageIndex = messageListPointer->GetLastMessageIndex();
}
