// ChatServer.cpp : Defines the entry point for the console application.
//

#undef UNICODE

#define _CRT_SECURE_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN

#include "stdafx.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <string>
#include "ChatServer.h"
#include <conio.h>
#include <tchar.h>
#include <vector>
#include "MessageList.h"
#include "Message.h"
#include "Client.h"
#include "ClientData.h"

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

#define BUF_SIZE 255
#define DEFAULT_PORT "27015"

using namespace std;

SOCKET openListenSocket()
{
	SOCKET ListenSocket = INVALID_SOCKET;

	int iResult;
	WSADATA wsaData;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return NULL;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return NULL;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return NULL;
	}

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return NULL;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return NULL;
	}

	return ListenSocket;
}

int __cdecl main(void)
{
	char recvbuf[BUF_SIZE];
	int recvbuflen = BUF_SIZE;
	MessageList messages = MessageList();
	SOCKET ListenSocket = INVALID_SOCKET;

	cout << "Starting Chat-Service server ..." << endl;

	ListenSocket = openListenSocket();

	// Create threads for receive and send of one client
	vector<DWORD> threadIds = vector<DWORD>();
	vector<HANDLE> threadHandles = vector<HANDLE>();
	vector<PCLIENTDATA> threadDataPointers = vector<PCLIENTDATA>();

	while (true) {
		SOCKET clientSocket = accept(ListenSocket, NULL, NULL);
		Client* client = new Client(clientSocket, &messages);

		// Start sending thread
		DWORD sendThreadId;
		threadHandles.push_back(CreateThread(NULL, 0, client->Send, client, 0, &sendThreadId));
		threadIds.push_back(sendThreadId);

		// Start receiving thread
		DWORD receiveThreadId;
		threadHandles.push_back(CreateThread(NULL, 0, client->Receive, client, 0, &receiveThreadId));
		threadIds.push_back(receiveThreadId);	
	}
	
	return 0;
}