#pragma once

#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <vector>
#include <string>

#include "Message.h"

#pragma comment (lib, "Ws2_32.lib")

using namespace std;

class MessageList
{
public:
	MessageList();
	~MessageList();

	void Add(string message, int id);
	vector<Message> GetLatest(int index);
	int GetLastMessageIndex();

private:
	vector<Message> messages;
	CRITICAL_SECTION criticalSection;
};

