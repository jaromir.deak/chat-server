#include "stdafx.h"
#include "Message.h"


Message::Message()
{
}

Message::Message(string str, int id)
{
	Message();
	SetText(str);
	SetId(id);
}


Message::~Message()
{
}

void Message::SetText(string str)
{
	text = str;
}

string Message::GetText()
{
	return text;
}

void Message::SetId(int id)
{
	this->id = id;
}

int Message::GetId()
{
	return this->id;
}
