#pragma once

#pragma comment (lib, "Ws2_32.lib")

#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <string>
#include "ChatServer.h"
#include <conio.h>
#include <tchar.h>
#include <vector>
#include "MessageList.h"
#include "Message.h"
#include "ClientData.h"

class Client
{
public:
	HANDLE stdoutHandle;
	SOCKET socket;
	MessageList* messageListPointer;
	
	Client();
	Client(SOCKET socket, MessageList* messages);
	~Client();

	void StoreMessage(string message);
	vector<Message> GetLatestMessages();
	int GetId();

	static DWORD WINAPI Receive(LPVOID lpParam);
	static DWORD WINAPI Send(LPVOID lpParam);

private:
	int lastMessageIndex;
	static string extractString(char* recvbuf, int size);
	void setLastMessageIndex();
};

